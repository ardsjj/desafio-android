package com.picpay.desafio.android.feature.usertlist

import com.picpay.desafio.android.RxImmediateSchedulerRule
import com.picpay.desafio.android.data.entity.User
import com.picpay.desafio.android.data.repository.user.UserRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class UserListPresenterTest {

    @Rule @JvmField val testSchedulerRule = RxImmediateSchedulerRule()
    private lateinit var subject: UserListPresenter
    private val view = mockk<UserListContract.View>(relaxed = true)
    private val repository = mockk<UserRepository>()
    private val successResponse = listOf<User>(mockk(), mockk())
    private val emptyResponse = emptyList<User>()
    private val failureResponse = Exception()

    @Before
    fun setUp() {
        subject = UserListPresenter(view, repository)
    }

    @Test
    fun `loads a valid user list from the repository and successfully displays`() {
        every { repository.getUserList() }.answers { Single.just(successResponse) }
        subject.loadUserList()
        verify(exactly = 1) { view.showUserList(successResponse) }
        verify(exactly = 0) { view.showError() }
    }

    @Test
    fun `loads an empty list from the repository and successfully displays`() {
        every { repository.getUserList() }.answers { Single.just(emptyResponse) }
        subject.loadUserList()
        verify(exactly = 1) { view.showUserList(emptyResponse) }
        verify(exactly = 0) { view.showError() }
    }

    @Test
    fun `when receiving an error loading data from the repository`() {
        every { repository.getUserList() }.answers { Single.error(failureResponse) }
        subject.loadUserList()
        verify(exactly = 0) { view.showUserList(emptyResponse) }
        verify(exactly = 1) { view.showError() }
    }
}