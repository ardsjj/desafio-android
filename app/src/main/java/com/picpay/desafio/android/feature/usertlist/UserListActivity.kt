package com.picpay.desafio.android.feature.usertlist

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.picpay.desafio.android.R
import com.picpay.desafio.android.data.entity.User
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class UserListActivity : AppCompatActivity(R.layout.activity_main), UserListContract.View {

    private val adapter: UserListAdapter by inject()
    private val layoutManager: LinearLayoutManager by inject()
    private val presenter: UserListContract.Presenter by inject { parametersOf(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.loadUserList()
        handleUI()
    }

    private fun handleUI() {
        toggleLoading()
        handleRefreshList()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager
    }

    private fun toggleLoading() {
        swipeRefresh.apply { isRefreshing = !isRefreshing }
    }

    override fun showUserList(userList: List<User>) {
        toggleLoading()
        adapter.userList = userList
    }

    override fun showError() {
        toggleLoading()
        val message = getString(R.string.error)
        Toast.makeText(this@UserListActivity, message, Toast.LENGTH_SHORT).show()
    }

    private fun handleRefreshList() {
        swipeRefresh.setOnRefreshListener {
            presenter.loadUserList()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.clear()
    }
}
