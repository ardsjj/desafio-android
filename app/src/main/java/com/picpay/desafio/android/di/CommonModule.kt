package com.picpay.desafio.android.di

import com.google.gson.GsonBuilder
import com.picpay.desafio.android.converter.UserConverter
import org.koin.dsl.module

val commonModule = module {
    factory {
        GsonBuilder().create()
    }

    factory {
        UserConverter(get())
    }
}