package com.picpay.desafio.android.feature.usertlist

import com.picpay.desafio.android.data.entity.User
import com.picpay.desafio.android.data.repository.user.UserRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class UserListPresenter(
    private val view: UserListContract.View,
    private val repository: UserRepository,
    private val disposable: CompositeDisposable = CompositeDisposable()
): UserListContract.Presenter {

    override fun loadUserList() {
        repository.getUserList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                this::handleLoadUserListSuccess,
                this::handleLoadUserListError
            ).run(disposable::add)
    }

    override fun clear() {
        disposable.dispose()
    }

    private fun handleLoadUserListSuccess(userList: List<User>) {
        view.showUserList(userList)
    }

    private fun handleLoadUserListError(throwable: Throwable) {
        view.showError()
    }
}