package com.picpay.desafio.android.feature.usertlist

import com.picpay.desafio.android.data.entity.User

interface UserListContract {
    interface View {
        fun showUserList(userList: List<User>)
        fun showError()
    }

    interface Presenter {
        fun loadUserList()
        fun clear()
    }
}