package com.picpay.desafio.android.di

import com.google.gson.Gson
import okhttp3.OkHttpClient
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val QUALIFIER_BASE_URL = named("BASE_URL")

val networkModule = module {

    factory(QUALIFIER_BASE_URL) {
        "http://careers.picpay.com/tests/mobdev/"
    }

    factory {
        OkHttpClient.Builder()
            .build()
    }

    factory {
        GsonConverterFactory.create(get<Gson>())
    }

    factory {
        RxJava2CallAdapterFactory.create()
    }

    factory {
        Retrofit.Builder()
            .baseUrl(get<String>(QUALIFIER_BASE_URL))
            .client(get<OkHttpClient>())
            .addConverterFactory(get<GsonConverterFactory>())
            .addCallAdapterFactory(get<RxJava2CallAdapterFactory>())
            .build()
    }
}