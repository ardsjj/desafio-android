package com.picpay.desafio.android.data.repository.user

import android.content.Context
import com.picpay.desafio.android.data.repository.user.source.local.UserLocalDataSource
import com.picpay.desafio.android.data.repository.user.source.remote.UserRemoteDataSource
import com.picpay.desafio.android.data.repository.user.source.remote.UserService
import org.koin.dsl.module
import retrofit2.Retrofit

val userRepositoryModule = module {

    factory {
        get<Retrofit>()
            .create(UserService::class.java)
    }

    factory {
        UserLocalDataSource(get(), get())
    }

    factory {
        UserRemoteDataSource(
            get()
        )
    }

    factory {
        UserRepository(
            remoteDataSource = get(),
            localDataSource = get())
    }

    factory {
        get<Context>().getSharedPreferences("app_preferences", Context.MODE_PRIVATE)
    }
}