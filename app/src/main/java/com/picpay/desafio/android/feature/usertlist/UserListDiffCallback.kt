package com.picpay.desafio.android.feature.usertlist

import androidx.recyclerview.widget.DiffUtil
import com.picpay.desafio.android.data.entity.User

class UserListDiffCallback(
    private val oldList: List<User>,
    private val newList: List<User>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].username == newList[newItemPosition].username &&
                oldList[oldItemPosition].infoOrigin == newList[newItemPosition].infoOrigin
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return true
    }
}