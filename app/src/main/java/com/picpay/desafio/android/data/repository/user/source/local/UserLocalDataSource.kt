package com.picpay.desafio.android.data.repository.user.source.local

import android.content.SharedPreferences
import com.picpay.desafio.android.converter.UserConverter
import com.picpay.desafio.android.data.entity.User
import io.reactivex.Single
import java.time.Duration
import java.time.LocalDateTime
import java.util.concurrent.TimeUnit

class UserLocalDataSource(
    private val sharedPreferences: SharedPreferences,
    private val userConverter: UserConverter,
    private val userListPreference: String = "user_list",
    private val lastUpdatePreference: String = "last_update") {

    fun save(userList: List<User>) {
        sharedPreferences
            .edit()
            .putString(userListPreference, userConverter.toJson(userList))
            .putString(lastUpdatePreference, LocalDateTime.now().toString())
            .apply()
    }

    fun getUserList(forceCache: Boolean = false): Single<List<User>> {
        val cache = sharedPreferences.getString(userListPreference, null)
        return if(shouldUseCache(cache, forceCache)) {
            userConverter.fromJson(cache.orEmpty())
                .map { it.copy(infoOrigin = "Local") }
                .let { Single.just(it) }
        } else {
            Single.error(ExpiredCacheException())
        }
    }

    private fun shouldUseCache(cache: String?, forceCache: Boolean): Boolean {
        return (!cache.isNullOrBlank() && !cacheExpired()) || (!cache.isNullOrBlank() && forceCache)
    }

    private fun cacheExpired(): Boolean {
        val last = LocalDateTime.parse(sharedPreferences.getString(lastUpdatePreference, null))
        return Duration.between(last, LocalDateTime.now()).toMillis() > TimeUnit.SECONDS.toMillis(40)
    }
}