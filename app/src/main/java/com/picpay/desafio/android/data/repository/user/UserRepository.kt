package com.picpay.desafio.android.data.repository.user

import com.picpay.desafio.android.data.entity.User
import com.picpay.desafio.android.data.repository.user.source.local.UserLocalDataSource
import com.picpay.desafio.android.data.repository.user.source.remote.UserRemoteDataSource
import io.reactivex.Single

class UserRepository(
    private val remoteDataSource: UserRemoteDataSource,
    private val localDataSource: UserLocalDataSource
) {

    fun getUserList(): Single<List<User>> {
        return localDataSource.getUserList()
            .onErrorResumeNext(this::handleInvalidCache)
            .onErrorResumeNext(localDataSource.getUserList(true))
    }

    private fun handleInvalidCache(error: Throwable): Single<List<User>> {
        return remoteDataSource
            .getUserList()
            .doOnSuccess(localDataSource::save)
    }
}
