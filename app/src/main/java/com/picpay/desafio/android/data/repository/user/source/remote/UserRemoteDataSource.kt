package com.picpay.desafio.android.data.repository.user.source.remote

import com.picpay.desafio.android.data.entity.User
import io.reactivex.Single

class UserRemoteDataSource(private val service: UserService) {

    fun getUserList(): Single<List<User>> {
        return service
            .getUsers()
            .map { userList ->
                userList.map { it.copy(infoOrigin = "Remote") }
            }
    }
}