package com.picpay.desafio.android.data.repository.user.source.remote

import com.picpay.desafio.android.data.entity.User
import io.reactivex.Single
import retrofit2.http.GET

interface UserService {
    @GET("users")
    fun getUsers(): Single<List<User>>
}