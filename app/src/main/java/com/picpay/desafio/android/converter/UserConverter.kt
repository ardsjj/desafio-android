package com.picpay.desafio.android.converter

import com.google.gson.Gson
import com.picpay.desafio.android.data.entity.User

class UserConverter(private val gson: Gson) {
    fun toJson(userList: List<User>): String
        = gson.toJson(userList)

    fun fromJson(userListJson: String)
            = gson.fromJson(userListJson, Array<User>::class.java).toList()
}