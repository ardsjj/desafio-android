package com.picpay.desafio.android.feature.usertlist

import androidx.recyclerview.widget.LinearLayoutManager
import org.koin.dsl.module

val userListModule = module {
    factory<UserListContract.Presenter> { (view: UserListContract.View) ->
        UserListPresenter(view, get())
    }

    factory {
        UserListAdapter()
    }

    factory {
        LinearLayoutManager(get())
    }
}