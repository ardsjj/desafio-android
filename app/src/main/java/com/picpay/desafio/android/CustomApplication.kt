package com.picpay.desafio.android

import android.app.Application
import com.picpay.desafio.android.data.repository.user.userRepositoryModule
import com.picpay.desafio.android.di.commonModule
import com.picpay.desafio.android.di.networkModule
import com.picpay.desafio.android.feature.usertlist.userListModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CustomApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CustomApplication)
            modules(arrayListOf(commonModule, networkModule, userRepositoryModule, userListModule))
        }
    }
}